package com.movie.movieapp

import BASE_URL
import DATABASE_NAME
import android.app.Application
import androidx.room.Room
import com.movie.movieapp.api.MoviesRetrofitApi
import com.movie.movieapp.persistence.MovieDB
import com.movie.movieapp.repository.MoviesRepository
import com.movie.movieapp.ui.MovieListViewModel
import com.movie.movieapp.utils.AppExecutors
import com.movie.movieapp.utils.LiveDataCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import CONNECTION_TIMEOUT

class BaseApplication : Application() {
    private val appModule = module {
        single<MoviesRetrofitApi> {
            val client = OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build()
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build().create(MoviesRetrofitApi::class.java)
        }
        viewModel {
            val repository: MoviesRepository = get()
            MovieListViewModel(
                repository, this@BaseApplication
            )
        }
        single {
            Room.databaseBuilder(
                this@BaseApplication.applicationContext, MovieDB::class.java,
                DATABASE_NAME
            ).build()
        }

        single {
            val movieDB: MovieDB = get()
            val appExecutors: AppExecutors = get()
            val moviesApi: MoviesRetrofitApi = get()
            MoviesRepository(movieDB.movieDao, appExecutors, moviesApi)
        }


        single {
            AppExecutors()
        }

    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(appModule)
        }
    }
}