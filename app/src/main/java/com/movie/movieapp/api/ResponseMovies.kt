package com.movie.movieapp.api

import com.google.gson.annotations.SerializedName
import com.movie.movieapp.models.MovieModel

class ResponseMovies {

    @SerializedName("page")
    var page: Int = 1

    @SerializedName("total_results")
    val totalResults: Int = 0

    @SerializedName("total_pages")
    val totalPages: Int = 0

    @SerializedName("results")
    val movies: List<MovieModel>? = null
}