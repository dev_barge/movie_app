package com.movie.movieapp.api

import androidx.lifecycle.LiveData
import com.movie.movieapp.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesRetrofitApi {

    @GET("movie/popular")
    fun getPopularMovies(
        @Query("page") page: Int = 1,
        @Query("api_key") apiKey: String = BuildConfig.API_KEY
    ): LiveData<MovieApiResponse<ResponseMovies>>
}