package com.movie.movieapp.api

import retrofit2.Response
import RESPONSE_CODE_204

sealed class MovieApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): MovieApiResponse<T> {
            return ApiErrorResponse(error.message ?: "Error")
        }

        fun <T> create(response: Response<T>): MovieApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == RESPONSE_CODE_204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(
                        body = body
                    )
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }
                ApiErrorResponse(errorMsg ?: "Error")
            }
        }
    }
}

class ApiEmptyResponse<T> : MovieApiResponse<T>()
data class ApiSuccessResponse<T>(val body: T) : MovieApiResponse<T>()
data class ApiErrorResponse<T>(val errorMessage: String) : MovieApiResponse<T>()