package com.movie.movieapp.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.movie.movieapp.models.MovieModel

@Database(
    entities = [MovieModel::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converter::class)
abstract class MovieDB : RoomDatabase() {
    abstract val movieDao: MovieAndDetailDao


}