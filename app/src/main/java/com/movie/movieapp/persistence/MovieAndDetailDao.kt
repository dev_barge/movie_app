package com.movie.movieapp.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.movie.movieapp.models.MovieModel
import com.movie.movieapp.utils.Category

@Dao
interface MovieAndDetailDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovies(vararg movie: MovieModel): LongArray

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: MovieModel)

    @Update
    fun updateMovie(movie: MovieModel)

    @Query("SELECT * FROM moviemodel WHERE category=:category LIMIT (:pageNumber*20)")
    fun getMovies(pageNumber: Int, category: Category): LiveData<List<MovieModel>>

    @Query("SELECT * FROM moviemodel WHERE title LIKE '%' || :query || '%'  LIMIT (:pageNumber*20) ")
    fun searchListMovie(query: String, pageNumber: Int): LiveData<List<MovieModel>>


}