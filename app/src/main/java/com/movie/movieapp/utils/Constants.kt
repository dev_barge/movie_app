const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/"
const val IMAGE_FILE_SIZE = "w185"
const val BASE_URL = "https://api.themoviedb.org/3/"
const val DATABASE_NAME = "movies.db"
const val RESPONSE_CODE_204 = 204
const val CONNECTION_TIMEOUT = 60L