package com.movie.movieapp.utils

enum class Category {
    POPULAR,
    UPCOMING,
    TOPRATED
}