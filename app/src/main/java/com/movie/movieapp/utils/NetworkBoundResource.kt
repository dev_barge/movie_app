package com.movie.movieapp.utils

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.movie.movieapp.api.ApiEmptyResponse
import com.movie.movieapp.api.ApiErrorResponse
import com.movie.movieapp.api.ApiSuccessResponse
import com.movie.movieapp.api.MovieApiResponse

abstract class NetworkBoundResource<CacheObject, RequestObject>
@MainThread constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<CacheObject>>()

    init {
        result.value = Resource.Loading(null)
        fetchFromNetwork()
    }


    @MainThread
    private fun setValue(newValue: Resource<CacheObject>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    appExecutors.diskIO.execute {
                        saveCallResult(processResponse(response))
                        appExecutors.mainThreadExecutor.execute {
                            result.addSource(loadFromDb()) { newCachedObject ->
                                setValue(Resource.Success(newCachedObject))
                            }

                        }
                    }
                }
                is ApiEmptyResponse -> {
                    appExecutors.mainThreadExecutor.execute {
                    }
                }
                is ApiErrorResponse -> {
                }
            }
        }
    }

    fun asLiveData() = result as LiveData<Resource<CacheObject>>

    @WorkerThread
    protected open fun processResponse(response: ApiSuccessResponse<RequestObject>) = response.body

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestObject?)

    @MainThread
    protected abstract fun shouldFetch(data: CacheObject?): Boolean

    @MainThread
    protected abstract fun createCall(): LiveData<MovieApiResponse<RequestObject>>

    @MainThread
    protected abstract fun loadFromDb(): LiveData<CacheObject>

}