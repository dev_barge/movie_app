package com.movie.movieapp.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.movie.movieapp.models.MovieModel
import com.movie.movieapp.repository.MoviesRepository
import com.movie.movieapp.utils.Category
import com.movie.movieapp.utils.Resource

class MovieListViewModel(private val repository: MoviesRepository, app: Application) :
    AndroidViewModel(app) {
    private var isQueryExhausted: Boolean = false
    private var cancelRequest = false

    private val _pageNumber = MutableLiveData<Int>()
    val pageNumber: LiveData<Int>
        get() = _pageNumber


    private val _movies = MediatorLiveData<Resource<List<MovieModel>>>()
    val movies: LiveData<Resource<List<MovieModel>>>
        get() = _movies

    init {
        _pageNumber.value = 1
    }

    fun getList() {
        executeRequest()
    }

    fun getNextPage() {
        if (!isQueryExhausted) {
            _pageNumber.value = _pageNumber.value?.plus(1)
            executeRequest()
        }
    }

    private fun executeRequest() {
        val repositorySource: LiveData<Resource<List<MovieModel>>> =
            repository.getListMovie(pageNumber.value!!, Category.POPULAR)
        registerMediatorLiveData(repositorySource)
    }


    private fun registerMediatorLiveData(repositorySource: LiveData<Resource<List<MovieModel>>>) {
        _movies.addSource(repositorySource) { resourceListMovie ->
            if (!cancelRequest) {
                if (resourceListMovie != null) {
                    _movies.value = resourceListMovie
                    if (resourceListMovie is Resource.Success || resourceListMovie is Resource.Error) {
                        unregisterMediatorLiveData(repositorySource)
                        resourceListMovie.data?.let {
                            if (it.size < _pageNumber.value!! * 10) {
                                isQueryExhausted = true
                            }
                        }
                    }
                } else {
                    unregisterMediatorLiveData(repositorySource)
                }
            } else {
                unregisterMediatorLiveData(repositorySource)
            }

        }
    }

    private fun unregisterMediatorLiveData(repositorySource: LiveData<Resource<List<MovieModel>>>) {
        _movies.removeSource(repositorySource)
    }

    fun resetPageNumber() {
        _pageNumber.value = 1
    }

    fun cancelRequest() {
        cancelRequest = true
        _pageNumber.value = 1
    }
}
