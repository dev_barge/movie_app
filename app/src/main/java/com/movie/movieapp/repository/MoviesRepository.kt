package com.movie.movieapp.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.movie.movieapp.api.MovieApiResponse
import com.movie.movieapp.api.MoviesRetrofitApi
import com.movie.movieapp.api.ResponseMovies
import com.movie.movieapp.models.MovieModel
import com.movie.movieapp.persistence.MovieAndDetailDao
import com.movie.movieapp.utils.AppExecutors
import com.movie.movieapp.utils.Category
import com.movie.movieapp.utils.NetworkBoundResource
import com.movie.movieapp.utils.Resource

class MoviesRepository(
    private val movieDao: MovieAndDetailDao,
    private val appExecutors: AppExecutors,
    private val movieApi: MoviesRetrofitApi
) {

    fun getListMovie(pageNumber: Int, category: Category): LiveData<Resource<List<MovieModel>>> {
        return object : NetworkBoundResource<List<MovieModel>, ResponseMovies>(appExecutors) {
            override fun saveCallResult(item: ResponseMovies?) {
                item?.let {
                    val list: ArrayList<MovieModel>? = (item.movies)?.let { ArrayList(it) }
                    val newList: ArrayList<MovieModel> = ArrayList()
                    list?.forEach {
                        val movie = it.copy(categoryType = category)
                        newList.add(movie)
                    }
                    newList.let {
                        movieDao.insertMovies(*newList.toTypedArray())
                    }
                }
            }

            override fun shouldFetch(data: List<MovieModel>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<MovieModel>> =
                movieDao.getMovies(pageNumber, category)

            override fun createCall(): LiveData<MovieApiResponse<ResponseMovies>> =
                when (category) {
                    Category.POPULAR -> movieApi.getPopularMovies(pageNumber)
                    else -> movieApi.getPopularMovies(pageNumber)
                }

        }.asLiveData()
    }


}